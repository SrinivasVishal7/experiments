import argparse
import cv2
import numpy as np
import scipy.misc
import skimage.color
import skimage.exposure
from PIL import Image
from skimage.measure import compare_mse as mse


def histBounds(histogram, margin=0.005):
    lowCut = int(len(histogram) * 0.12)
    highCut = int(len(histogram) * 0.95)
    histCut = histogram[lowCut:highCut]
    logMargin = np.max(histCut) * margin
    nonzero = np.where(histCut > logMargin)
    return lowCut + np.min(nonzero), lowCut + np.max(nonzero)


def normalizeChannel(channel, maxVal=255.):
    arr = channel.astype(float)
    hist, bounds = np.histogram(arr, maxVal)

    min, max = histBounds(hist)

    arr = (float(maxVal) / float(max - min)) * (arr - min)

    return np.clip(arr, 0, maxVal).astype(channel.dtype)


def normalizeLab(arr):
    lab = skimage.color.rgb2lab(arr / 255.)

    lab[..., 0] = normalizeChannel(lab[..., 0], 100)

    rgb = skimage.color.lab2rgb(lab)
    rgb = (rgb * 255).astype(int)
    return rgb


def normalizeImage(img):
    bytes = scipy.misc.fromimage(img)
    bytes = normalizeLab(bytes)
    return scipy.misc.toimage(bytes)


def normalizeImageChannel(img):
    bytes = scipy.misc.fromimage(img)
    for i in range(3):
        bytes[..., i] = normalizeChannel(bytes[..., i], 255)
    return scipy.misc.toimage(bytes)


def adjust_brightness(image, brightness=1.0):
    invgamma = 1.0 / brightness
    table = np.array([((i / 255.0) ** invgamma) * 255 for i in np.arange(0, 256)]).astype("uint8")
    return cv2.LUT(image, table)


def histogram_equalize(image, choice):
    if choice == 0:
        equalized = cv2.equalizeHist(image)
        return equalized
    else:
        clahe = cv2.createCLAHE(clipLimit=0.0, tileGridSize=(4, 4))
        equalized = clahe.apply(image)
        return equalized


def meansquaredLoss(imageA, imageB):
    err = mse(imageA, imageB)
    return err


def norm_hist(imageA, imageB):
    labA = skimage.color.rgb2lab(imageA / 255.)
    labB = skimage.color.rgb2lab(imageB / 255.)
    labB[..., 0] = labA[..., 0]
    rgbB = skimage.color.lab2rgb(labB)
    rgbB = (rgbB * 255).astype(int)
    return rgbB


ap = argparse.ArgumentParser()
ap.add_argument("-b", "--bright", default=1, help="enter brightness")
ap.add_argument("-c", "--choice", default=0, help="Enter choice")
ap.add_argument("-p", "--path", required=True, help="enter Image path")
ap.add_argument("-s", "--space", default=0, help="Enter color space 0 - YCbCr, 1- LAB, 2-HSV")

args = vars(ap.parse_args())

brightness = float(args["bright"])
choice = int(args["choice"])
image = args["path"]
images = image.split(',')
c_space = int(args["space"])

space = {
    0: cv2.COLOR_BGR2YCrCb,
    1: cv2.COLOR_BGR2LAB
}

space_inverse = {
    0: cv2.COLOR_YCrCb2BGR,
    1: cv2.COLOR_LAB2BGR
}

original = cv2.imread(images[0])
brightened = cv2.imread(images[1]) if len(images) > 1 else adjust_brightness(original, brightness)

image_hcombine_1 = np.hstack([original, brightened])

im_original = Image.open(images[0])
im_changed = Image.open(images[1])

im_normalized = norm_hist(im_original, im_changed)
im_normalized = np.array(im_normalized)
im_normalized = im_normalized[:, :, ::-1].copy()

image_hcombine_2 = np.hstack([image_hcombine_1, im_normalized])

cv2.imshow("Images")


# im_original = Image.open(images[0])
# im_original = normalizeImageChannel(im_original)
# equalized_original = np.array(im_original)
# equalized_original = equalized_original[:, :, ::-1].copy()
#
# im_changed = Image.open(images[1])
# im_changed = normalizeImageChannel(im_changed)
# equalized_changed = np.array(im_changed)
# equalized_changed = equalized_changed[:, :, ::-1].copy()

# original_new = cv2.cvtColor(original, space[c_space])
# brightened_new = cv2.cvtColor(brightened, space[c_space])
#
# original_new_split_light, original_new_split_ch1, original_new_split_ch2 = cv2.split(original_new)
# brightened_new_split_light, brightened_new_split_ch1, brightened_new_split_ch2 = cv2.split(brightened_new)
#
# image_hcombine_4 = np.hstack([original_new_split_light, brightened_new_split_light])
# cv2.imshow("original Light images", image_hcombine_4)
#
#
# equalized_original_light = histogram_equalize(original_new_split_light, choice)
# equalized_brightened_light = histogram_equalize(brightened_new_split_light, choice)
#
# image_hcombine_3 = np.hstack([equalized_original_light, equalized_brightened_light])
# cv2.imshow("Equalized Light images", image_hcombine_3)
#
# equalized_original = cv2.merge((equalized_original_light, original_new_split_ch1, original_new_split_ch2))
# equalized_brightened = cv2.merge((equalized_brightened_light, brightened_new_split_ch1, brightened_new_split_ch2))
#
# equalized_original = cv2.cvtColor(equalized_original, space_inverse[c_space])
# equalized_brightened = cv2.cvtColor(equalized_brightened, space_inverse[c_space])

# image_hcombine_2 = np.hstack([equalized_original, equalized_changed])
# image_vcombined = np.vstack([image_hcombine_1, image_hcombine_2])

cv2.imshow('Images', image_vcombined)

print("\n------------------------------------------------------------------------------------")
print("\nMean squared Loss between original and light variant image : {}".format(meansquaredLoss(original, brightened)))
print("\nMean squared Loss between original and Normalized image : {}".format(
    meansquaredLoss(original, im_normalized)))
print("\nMean squared loss between lightened and normalized : {}".format(meansquaredLoss(brightened, im)))

# print("\nStructural Similarity between both original and light variant image : {}".format( ssim(
# original_new_split_light, brightened_new_split_light))) print("\nStructural Similarity between both transformed
# original and light variant image : {}".format( ssim(equalized_original_light, equalized_brightened_light))) print(
# "\nMean squared error of other two channels of original and light variant Image : {},{}".format(meansquaredLoss(
# original_new_split_ch1,brightened_new_split_ch1), meansquaredLoss(original_new_split_ch2,
# brightened_new_split_ch2))) print("\nMean squared Loss of original Light channels : {}".format(meansquaredLoss(
# original_new_split_light,brightened_new_split_light))) print("\nMean squared loss of equalized light channels : {
# }".format(meansquaredLoss(equalized_original_light,equalized_brightened_light))) print(
# "\n------------------------------------------------------------------------------------")

cv2.waitKey(0)
