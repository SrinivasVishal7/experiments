import cv2
import numpy as np
from normalize_image import NormalizedRGB
from matplotlib import pyplot as plt
import argparse

def adjust_brightness(image, gamma=1.0):
	invGamma = 1.0 / gamma
	table = np.array([((i / 255.0) ** invGamma) * 255
		for i in np.arange(0, 256)]).astype("uint8")
	return cv2.LUT(image, table)

ap = argparse.ArgumentParser()
ap.add_argument("-b", "--bright", required=True,
	help="enter brightness")
args = vars(ap.parse_args())
brightness = float(args["bright"])

n=NormalizedRGB()

original = cv2.imread('test.bmp')
brightened = adjust_brightness(original,brightness)

image_1 = cv2.cvtColor(original,cv2.COLOR_BGR2LAB)
image_2 = cv2.cvtColor(brightened,cv2.COLOR_BGR2LAB)

cv2.imshow('Images',np.hstack([original,brightened]))
#plt.plot(light_array)
cv2.imshow('LAB for original',np.hstack([original,image_1]))
cv2.imshow('LAB for Brightened',np.hstack([brightened, image_2]))
#img = cv2.cvtColor(img1,cv2.COLOR_BGR2HSV)
#n.getRGB(img)
#norm=n.normalized()
#cv2.imshow('Un Normalized Image',img1)
#norm =  cv2.cvtColor(norm,cv2.COLOR_HSV2BGR_FULL)
#cv2.imshow('Normalized Image',norm)
cv2.waitKey()
#plt.show()

