import cv2
import numpy as np

class NormalizedRGB:
    def __init__(self):
        self.channels=np.zeros((224,224,3),np.uint8)
        self.norm=np.zeros((224,224,3),np.float32)
        self.norm_rgb=np.zeros((224,224,3),np.uint8)
        self.down=np.zeros((224,224,3),np.float32)

    def getChannels(self,rgb):
        self.channels=channels
        self.down[:,:,:]=self.channels[:,:,:]
        print(self.down.shape)

    def normalized(self):
        b=self.down[:,:,0]
        g=self.down[:,:,1]
        r=self.down[:,:,2]
        sum=b+g+r
        self.norm[:,:,0]=b/sum*255.0
        self.norm[:,:,1]=g/sum*255.0
        self.norm[:,:,2]=r/sum*255.0
        #self.norm=cv2.merge([self.norm1,self.norm2,self.norm3])
        self.norm_rgb=cv2.convertScaleAbs(self.norm)
        #self.norm.dtype=np.uint8
        return self.norm_rgb
