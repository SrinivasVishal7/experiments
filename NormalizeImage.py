import cv2
import numpy as np
import scipy.misc

import skimage.color
import skimage.exposure
from PIL import Image


def histBounds(histogram, margin=0.005):
    lowCut = int(len(histogram) * 0.12)
    highCut = int(len(histogram) * 0.95)
    histCut = histogram[lowCut:highCut]
    logMargin = np.max(histCut) * margin
    nonzero = np.where(histCut > logMargin)
    return lowCut + np.min(nonzero), lowCut + np.max(nonzero)


def normalizeChannel(channel, maxVal=255.):
    arr = channel.astype(float)
    hist, bounds = np.histogram(arr, maxVal)

    min, max = histBounds(hist)

    arr = (float(maxVal) / float(max - min)) * (arr - min)

    return np.clip(arr, 0, maxVal).astype(channel.dtype)


def normalizeLab(arr):
    lab = skimage.color.rgb2lab(arr / 255.)

    lab[..., 0] = normalizeChannel(lab[..., 0], 100)

    rgb = skimage.color.lab2rgb(lab)
    rgb = (rgb * 255).astype(int)
    return rgb


def normalizeImage(img):
    bytes = scipy.misc.fromimage(img)
    bytes = normalizeLab(bytes)
    return scipy.misc.toimage(bytes)


def normalizeImageChannel(img):
    bytes = scipy.misc.fromimage(img)
    for i in range(3):
        bytes[..., i] = normalizeChannel(bytes[..., i], 255)
    return scipy.misc.toimage(bytes)


im = Image.open("test.bmp")
cv2.imshow("Original", cv2.imread("test.bmp"))
im = normalizeImageChannel(im)
pil_image = im
open_cv_image = np.array(pil_image)
open_cv_image = open_cv_image[:, :, ::-1].copy()
cv2.imshow("normalized", open_cv_image)
cv2.waitKey(0)
