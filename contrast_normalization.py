import numpy
import scipy
import scipy.misc
from PIL import Image
import cv2


def global_contrast_normalization(filename, s, lmda, epsilon):
    X = numpy.array(Image.open(filename))

    # replacement for the loop
    X_average = numpy.mean(X)
    print('Mean: ', X_average)
    X = X - X_average

    # `su` is here the mean, instead of the sum
    contrast = numpy.sqrt(lmda + numpy.mean(X**2))

    X = s * X / max(contrast, epsilon)

    # scipy can handle it
    cv2.imshow('result', X)
    cv2.waitKey()


global_contrast_normalization("test.bmp", 1, 10, 0.000000001)
