import cv2
import numpy as np
from keras import Model
from keras.applications import VGG19
from sklearn.externals import joblib
from keras.preprocessing import image
from keras.applications.imagenet_utils import preprocess_input
import argparse

from pyimagesearch.preprocessing import ExtractPreprocessor

crop_w = 224
crop_h = 224

MARGIN = 50

cam = cv2.VideoCapture(0)
if not cam.isOpened():
    print('camera not opened')
    exit()
else:
    print('camera opened')

ap = argparse.ArgumentParser()
ap.add_argument("-p", "--Path", required=True,
                help="path to model")
ap.add_argument('-n', "--name", required=True,
                help='Name of the pill')
args = vars(ap.parse_args())

base_model = VGG19(weights='imagenet')
model = Model(input=base_model.input,
              output=base_model.get_layer('block5_pool').output)
print("\nLoading VGG19 pre-trained model...\n")

cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

lsanomaly_model = joblib.load(args['Path'])

labels = [args['name'], 'Not ' + args['name']]
print('classes : ', len(labels))
score = [None] * (len(labels))
print('labels : ', labels)

NUM_CLASSES = len(labels)

imageCount = 0

for skip in range(10):
    ret, image_cam = cam.read()
    if ret is False:
        print("image not found")
        exit()
imgX, imgY = image_cam.shape[1] / 2, image_cam.shape[0] / 2

croppedImage = image_cam

first_gray = cv2.cvtColor(croppedImage, cv2.COLOR_BGR2GRAY)
extImage = ExtractPreprocessor(GRAYBaseFrame=first_gray, thresholdValue=10, H=crop_h, W=crop_w)


def prediction(image_cam):
    tempImage = image_cam.copy()

    tempImage[int(imgY) - int(crop_h / 2), int(imgX) - int(crop_w / 2):int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) + int(crop_h / 2), int(imgX) - int(crop_w / 2):int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) - int(crop_h / 2):int(imgY) + int(crop_h / 2), int(imgX) + int(crop_w / 2)] = 0
    tempImage[int(imgY) - int(crop_h / 2):int(imgY) + int(crop_h / 2), int(imgX) - int(crop_w / 2)] = 0
    status, extractedImage = extImage.preprocess(image_cam)
    cv2.imshow("extractedImage", extractedImage)

    image_extracted = extractedImage.copy()

    if NUM_CLASSES < 20:
        num_row = int(NUM_CLASSES % 20)
    else:
        num_row = 19
    canvas = np.zeros((35 * (1 + num_row) + 5, 350 * (1 + int(NUM_CLASSES / 20)), 3), dtype="uint8")

    #   image_extracted = np.expand_dims(image_extracted, axis=0)
    img = image.img_to_array(image_extracted)
    img = np.expand_dims(img, axis=0)
    img = preprocess_input(img)
    features = model.predict(img).flatten()
    Y = np.array(features)
    Y = Y.reshape(1, -1)
    preds = lsanomaly_model.predict_proba(Y)
    pred = np.argmax(preds, axis=1)
    for cl in range(len(labels)):
        val = preds[0][cl] * 100
        score[cl] = round(val, 2)
    best_score = score[int(pred)]

    for cl in range(len(labels)):
        w = int(score[cl] * 3)
        if cl == pred:
            cv2.rectangle(canvas, (5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 5),
                          (w + 5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 35), (255, 0, 0), -1)
        else:
            cv2.rectangle(canvas, (5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 5),
                          (w + 5 + 350 * int(cl / 20), (int(cl % 20) * 35) + 35), (0, 0, 255), -1)
        cv2.putText(canvas, labels[int(cl)] + ' ' + str(score[cl]), (10 + 400 * int(cl / 20), (int(cl % 20) * 35) + 23),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.45, (255, 255, 255), 1)
    cv2.putText(tempImage, labels[int(pred)] + ' ' + str(best_score), (300, 50), 0, 0.8, (255, 0, 0), 2, cv2.LINE_AA)

    cv2.imshow('croppedImage', croppedImage)
    cv2.imshow('probabilities', canvas)
    cv2.imshow('frame', tempImage)
    k = cv2.waitKey(1)
    if chr(k & 255) is 'q':
        exit()


while True:
    ret, image_from_cam = cam.read()
    if ret is False:
        print("image not found")
        exit()
    prediction(image_from_cam)
