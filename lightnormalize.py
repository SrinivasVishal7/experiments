import cv2
import numpy as np

source_image = cv2.imread('test.bmp')
source_cvt = cv2.cvtColor(source_image,cv2.COLOR_BGR2LAB)

template_image = cv2.imread('Gelusil_MPS_17_112.bmp')
template_cvt = cv2.cvtColor(template_image,cv2.COLOR_BGR2LAB)

light_source,A_source,B_source = cv2.split(source_cvt)
light_template,A_template,B_template = cv2.split(template_cvt)

Output_image = np.zeros_like(source_image)

Output_image[:,:,0] = light_template
Output_image[:,:,1] = A_source
Output_image[:,:,2] = B_source

out_img = cv2.cvtColor(Output_image, cv2.COLOR_LAB2BGR)
image_hcombine = np.hstack([source_image,template_image,out_img])
cv2.imshow("output",image_hcombine)
cv2.waitKey(0)