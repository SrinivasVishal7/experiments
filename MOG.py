import cv2
import numpy as np

crop_w = 224
crop_h = 224

cam = cv2.VideoCapture(0)

if not cam.isOpened():
    print('camera not opened')
    exit()
else:
    print('camera opened')

cam.set(cv2.CAP_PROP_FRAME_WIDTH, 1280)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)

fgbg = cv2.createBackgroundSubtractorMOG2(history=500, varThreshold=16, detectShadows=False)
finalImage_count = 0
while True:
    ret, RGBFrame = cam.read()
    largest_area = 0
    largest_perimeter = 0
    GRAYFrame = cv2.cvtColor(RGBFrame, cv2.COLOR_BGR2GRAY)
    fgmask = fgbg.apply(GRAYFrame)
    kernel = np.ones((2, 2), np.uint8)
    eroded_frame_1 = cv2.erode(fgmask, kernel, iterations=3)
    dilated_frame = cv2.dilate(eroded_frame_1, kernel, iterations=3)
    eroded_frame_2 = dilated_frame.copy()
    res = cv2.bitwise_and(RGBFrame, RGBFrame, mask=eroded_frame_2)
    cv2.imshow('res', res)
    res_x_cen = int(len(res[0]) / 2)
    res_y_cen = int(len(res) / 2)
    center_crop = RGBFrame[res_y_cen - int(224 / 2):res_y_cen + int(224 / 2),
                  res_x_cen - int(224 / 2):res_x_cen + int(224 / 2)]
    contours = cv2.findContours(eroded_frame_2.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[1]
    x = y = w = h = 0
    for i, cnt in enumerate(contours):
        area = cv2.contourArea(cnt)
        perimeter = cv2.arcLength(cnt, True)
        if area > largest_area:
            x, y, w, h = cv2.boundingRect(cnt)
            largest_area = area
            # largest_perimeter = perimeter

        # print("Contour Area : {}".format(area))
        # print("Contour Perimeter : {}".format(perimeter))
        x_center = int(x + w / 2)
        y_center = int(y + h / 2)

        x1, x2 = x_center - int(224 / 2), x_center + int(224 / 2)
        y1, y2 = y_center - int(224 / 2), y_center + int(224 / 2)

        if x1 < 0:
            x1 = 0
        if y1 < 0:
            y1 = 0
        if x2 >= len(GRAYFrame[0]):
            x2 = len(GRAYFrame[0]) - 1
        if y2 >= len(GRAYFrame):
            y2 = len(GRAYFrame) - 1
        finalImage = res[y1:y2, x1:x2]
        if y2 - y1 is 224 and x2 - x1 is 224:
            finalImage = finalImage.copy()
        else:
            finalImage = center_crop.copy()
        cv2.imshow("Final Image", finalImage)
    k = cv2.waitKey(30) & 0xff
    if k == 27:
        break
cam.release()
cv2.destroyAllWindows()
